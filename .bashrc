#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

parse_git_branch() {
	git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
PS1='\[\e]0;\u@\h: \w\a\]\[\033[1;92m\]\u@\h\[\033[m\]:\[\033[1;94m\]\w\[\033[m\]\n\$ '

#PS1='\[\e[1;32m\u\e[1;37m@\e[1;33m\W \e[0;36m$(parse_git_branch) \e[0m\]'

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

#auto cd without typing cd
shopt -s autocd

# If there are multiple matches for completion, Tab should cycle through them
bind 'TAB':menu-complete
bind '"\e[Z": menu-complete-backward'

# Display a list of the matching files
bind "set show-all-if-ambiguous on"

# Perform partial completion on the first Tab press,
# only start cycling full results on the second Tab press
bind "set menu-complete-display-prefix on"
bind "set completion-ignore-case on"

# # Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
   . /usr/share/bash-completion/bash_completion

#setting "nvim" as manpager
#export MANPAGER="nvim -c 'set ft=man' -"
export EDITOR=nvim
export VISUAL=nvim
export FZF_DEFAULT_COMMAND="rg --files --hidden -g '!{node_modules,.git,dist,bin,build,admin_app}'"
#Movies/ ~/Entertainment/Animation/ ~/Entertainment/serials/
media(){
	mpv --really-quiet "$(find ~/Downloads/Entertainment/ -not -name "*.srt" -not -name "*.jpg" -type f | fzf)"
}

fcd(){
	cd $(find ~ -maxdepth 8 -type d \( -name node_modules -o -name .next -o -name .git -o -name .eclipse -o -name libreoffice -o -name google-chrome -o -name .cache -o -name .npm -o -name chromium -o -name build -o -name dotfiles -o -name src \) -prune -o -type d -print | fzf)
}
#fzf | xargs -d '\n' mpv


if [[ -e ~/.ssh/ipaddr ]]; then
  source ~/.ssh/ipaddr
fi

# Changing "ls" to "exa"
alias ls='exa -la --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

#sensitivity
alias grep="grep -i"
alias rg="rg -i"

#navigation aliase
alias .="cd"
alias ..="cd .."
alias ...="cd ../../"

#coding aliase
alias cod="cd ~/Documents/coding/"
alias dev="head -n 10 ~/MEGAsync/docs/devOps-notes.txt"
alias fina="cd ~/Documents/coding/financeApp/finance-app/ && nvim"
alias fins="cd ~/Documents/coding/financeApp/finance-server/ && nvim"

# vim = nvim
alias vim='nvim'

#git aliase
alias addall='git add .'
alias commit='git commit -m'
alias push='git push origin'
alias dotfiles='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

#system aliases
alias prf='sudo shutdown now'
alias rbt='reboot'
alias fyi='~/Downloads/raw/my.sh'

#pacman aliases
alias pacsrc='pacman -Ss'
alias pacins='sudo pacman -S'
alias pacup='sudo pacman -Syyu'

#abbrevations to open configs in nvim
alias cala="nvim ~/.config/alacritty/alacritty.yml"
alias czs="nvim ~/.zshrc"
alias cbs="nvim ~/.bashrc"
alias cqt="nvim ~/.config/qtile/config.py"
alias cvi="nvim ~/.config/nvim/init.lua"
alias cmo="nvim ~/.xmonad/xmobarrc"
alias cmd="nvim ~/.xmonad/xmonad.hs"

#abbrevations to cd into dir
alias fala="cd ~/.config/alacritty/"
alias fqt="cd ~/.config/qtile/"
alias fvi="cd ~/.config/nvim/"
alias fmd="cd ~/.xmonad/"


#if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then exec startx; fi
export PATH=~/go/bin:/usr/local/go/bin:$PATH


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
source "$HOME/.cargo/env"

alias luamake=/home/aun/Downloads/lua-language-server/3rd/luamake/luamake
