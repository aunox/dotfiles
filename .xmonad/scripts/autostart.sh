#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

#cursor active at boot
xsetroot -cursor_name left_ptr &

#starting utility applications at boot time
run nm-applet &
nitrogen --restore &
#run xfce4-power-manager &
run volumeicon &
picom &
#blueberry-tray &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 --tint 0x282c34  --height 22 &
#xcape -e 'Hyper_L=Tab;Hyper_R=backslash' &
