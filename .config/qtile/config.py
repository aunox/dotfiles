# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import socket
import subprocess
from libqtile.config import Click, Drag, Group, Key, Screen
from libqtile import bar, layout, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401
import psutil

mod = "mod4"
myTerm = "alacritty"

keys = [

    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down(), desc="Move focus down in stack pane"),
    Key([mod], "j", lazy.layout.up(), desc="Move focus up in stack pane"),
    Key(["mod1"], "space", lazy.spawn("dmenu_run -p 'Run: '"), desc='Dmenu Run Launcher'),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down(), desc="Move window down in current stack "),
    Key([mod, "control"], "j", lazy.layout.shuffle_up(), desc="Move window up in current stack "),

    #Key([mod2], "Print", lazy.spawn('xfce4-screenshooter')),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next(), desc="Switch window focus to other pane(s) of stack"),

    #App startups
    Key([mod], "f", lazy.spawn("firefox"), desc="lauches firefox"),
    Key([mod], "g", lazy.spawn("google-chrome-stable"), desc="lauches google"),
    Key([mod, "shift"], "p", lazy.spawn("pamac-manager"), desc="lauches pamac-manager"),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate(), desc="Swap panes of split stack"),
    Key([mod], "m", lazy.layout.maximize(), desc='toggle window between minimum and maximum sizes'),
    Key([mod, "shift"], "f", lazy.window.toggle_floating(), desc='toggle floating'),
    #Key([mod], "e", lazy.spawn(myTerm + " -e ranger"), desc='ranger file manager'),
    Key([mod], "e", lazy.spawn(myTerm+" -e vifm"), desc='vifm'),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "t", lazy.spawn(myTerm), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown qtile"),
    Key(
        [mod], "XF86AudioRaiseVolume",
        lazy.spawn("amixer -c 0 -q set Master 2dB+")
    ),
    Key(
        [mod], "XF86AudioLowerVolume",
        lazy.spawn("amixer -c 0 -q set Master 2dB-")
    ),
    Key(
        [], "XF86AudioMute",
        lazy.spawn("amixer -c 0 -q set Master toggle")
    )
]

group_names = [("WWW", {'layout': 'monadtall'}),
               ("DEV", {'layout': 'monadtall'}),
               ("SYS", {'layout': 'monadtall'}),
               ("DOC", {'layout': 'floating'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {"border_width": 2,
                "margin": 6,
                "border_focus": "#e1acff",
                "border_normal": "#1D2330"
                }

layouts = [
    #layout.MonadWide(**layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    #layout.Zoomy(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    layout.Tile(shift_windows=True, **layout_theme),
    layout.Stack(num_stacks=2,
        border_width=2,
        margin=6,
        border_focus="#e1acff",
        border_normal="#1D2330"
        )
    ]

#widget_defaults = dict(
#    font='sans',
#    fontsize=12,
#    padding=3,
#)
colors = [["#292d3e", "#292d3e"], # panel background
          ["#434758", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#8d62a9", "#8d62a9"], # border line color for other tab and odd widgets
          ["#668bd7", "#668bd7"], # color for the even widgets
          ["#e1acff", "#e1acff"]] # window name
prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

widget_defaults = dict(
    font="Ubuntu Mono",
    fontsize = 12,
    padding = 2,
    background=colors[2]
)


extension_defaults = widget_defaults.copy()


screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                       linewidth = 0,
                       padding = 10,
                       foreground = colors[4],
                       background = colors[0]
                       ),
                widget.Image(
                       filename = "~/.config/qtile/icons/bar-icon.png",
                       ),
                widget.Sep(
                       linewidth = 0,
                       padding = 5,
                       foreground = colors[2],
                       background = colors[0]
                       ),
                widget.GroupBox(
                       font = "Ubuntu Bold",
                       fontsize = 9,
                       margin_y = 5,
                       margin_x = 0,
                       padding_y = 5,
                       padding_x = 3,
                       borderwidth = 3,
                       active = colors[2],
                       inactive = colors[2],
                       rounded = False,
                       highlight_color = colors[1],
                       highlight_method = "block",
                       this_current_screen_border = colors[3],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[0],
                       other_screen_border = colors[0],
                       foreground = colors[2],
                       background = colors[0],
                       ),
                widget.Sep(
                       linewidth = 0,
                       padding = 20,
                       foreground = colors[2],
                       background = colors[0]
                       ),
                widget.WindowName(
                        foreground = "#ecbe7b",
                       background = colors[0],
                       padding = 0
                       ),
                widget.TextBox(
                       text = " 🌡",
                       padding = 2,
                       foreground = "#b3afc2",
                       background = colors[0],
                       fontsize = 11
                       ),
                widget.ThermalSensor(
                       foreground = "#b3afc2",
                       background = colors[0],
                       threshold = 90,
                       padding = 5
                       ),
                widget.TextBox(
                       text = '|',
                       foreground = "#666666",
                       background = colors[0],
                       padding = 5,
                       ),
                widget.TextBox(
                       text = " ⟳",
                       padding = 2,
                       foreground = "#ecbe7b",
                       background = colors[0],
                       fontsize = 14
                       ),
                widget.Pacman(
                       update_interval = 1800,
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e sudo pacman -Syu')},
                       foreground = "#ecbe7b",
                       background = colors[0],
                       ),
                widget.TextBox(
                       text = "Updates",
                       padding = 5,
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e sudo pacman -Syu')},
                       foreground = "#ecbe7b",
                       background = colors[0],
                       ),
                widget.TextBox(
                       text = '|',
                       foreground = "#666666",
                       background = colors[0],
                       padding = 5,
                       ),
                widget.TextBox(
                       text = " 🖬",
                       foreground = "#ff6c6b",
                       background = colors[0],
                       padding = 0,
                       fontsize = 14
                       ),
                widget.Memory(
                       foreground = "#ff6c6b",
                       background = colors[0],
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e htop')},
                       ),
                widget.TextBox(
                       text = '|',
                       foreground = "#666666",
                       background = colors[0],
                       padding = 5,
                       ),
                widget.Net(
                       interface = "eno1",
                       format = '{down} ↓↑ {up}',
                       foreground = "#51afef",
                       background = colors[0],
                       ),
                widget.TextBox(
                       text = '|',
                       foreground = "#666666",
                       background = colors[0],
                       padding = 5,
                       ),
                widget.TextBox(
                      text = " Vol:",
                       foreground = "#98be65",
                       background = colors[0],
                       padding = 0
                       ),
                widget.Volume(
                       foreground = "#98be65",
                       background = colors[0],
                       padding = 5
                       ),
                widget.TextBox(
                       text = '|',
                       foreground = "#666666",
                       background = colors[0],
                       padding = 5,
                       volume_down_command= "XF86AudioLowerVolume"
                       ),
                widget.CurrentLayoutIcon(
                       custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                       foreground = "#c678dd",
                       background = colors[0],
                       padding = 0,
                       scale = 0.6
                       ),
                widget.CurrentLayout(
                       #foreground = "#d3869b",
                       foreground = "#c678dd",
                       background = colors[0],
                       padding = 0
                       ),
                widget.TextBox(
                       text = '|',
                       foreground = "#666666",
                       background = colors[0],
                       padding = 5,
                       ),
                widget.Battery(
                        foreground = "#46d9ff",
                        background = colors[0],
                        low_foreground = "#ff6c6b",
                        format = "{char} {percent:2.0%} {hour:d}:{min:02d}",
                        error_message = "Error"
                        # format= "{char} {percent:2.0%} {hour:d}:{min:02d}",
                        ),
                widget.TextBox(
                       text = '|',
                       foreground = "#666666",
                       background = colors[0],
                       padding = 5,
                       ),
                widget.Clock(
                       foreground = "#46d9ff",
                       background = colors[0],
                       format = "%a, %b%d [ %I:%M%p ]",
                       ),
                widget.Sep(
                       linewidth = 0,
                       padding = 10,
                       foreground = colors[4],
                       background = colors[0]
                       ),
                widget.Systray(
                       background = colors[0],
                       padding = 5
                       ),
                widget.Sep(
                       linewidth = 0,
                       padding = 10,
                       foreground = colors[4],
                       background = colors[0]
                       ),
                ],
                opacity=1.0, size=20,
            ),
        ),
    ]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_types = ["notification", "toolbar", "splash", "dialog"]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'Arcolinux-welcome-app.py'},
    {'wmclass': 'Arcolinux-tweak-tool.py'},
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'mn-applet'},
    {'wmclass': 'megasync'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},
    {'wmclass': 'makebranch'},
    {'wmclass': 'maketag'},
    {'wmclass': 'Arandr'},
    {'wmclass': 'feh'},
    {'wmclass': 'Galculator'},
    {'wmclass': 'arcolinux-logout'},
    {'wmclass': 'xfce4-terminal'},
    {'wname': 'branchdialog'},
    {'wname': 'Open File'},
    {'wname': 'pinentry'},
    {'wmclass': 'ssh-askpass'},

],  fullscreen_border_width = 0, border_width = 0)

auto_fullscreen = True
focus_on_window_activation = "smart"


@hook.subscribe.client_new
def _swallow(window):
    pid = window.window.get_net_wm_pid()
    ppid = psutil.Process(pid).ppid()
    cpids = {c.window.get_net_wm_pid(): wid for wid, c in window.qtile.windows_map.items()}
    for i in range(5):
        if not ppid:
            return
        if ppid in cpids:
            parent = window.qtile.windows_map.get(cpids[ppid])
            parent.minimized = True
            window.parent = parent
            return
        ppid = psutil.Process(ppid).ppid()

@hook.subscribe.client_killed
def _unswallow(window):
    if hasattr(window, 'parent'):
        window.parent.minimized = False

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie work
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
