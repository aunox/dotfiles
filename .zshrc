# Use powerline
USE_POWERLINE="true"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
set -o vi

bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

#setting "nvim" as manpager
export MANPAGER="nvim -c 'set ft=man' -"
export EDITOR=nvim
export VISUAL=nvim
autoload edit-command-line; zle -N edit-command-line
bindkey -M vicmd v edit-command-line

if [[ -e ~/.ssh/ipaddr ]]; then
  source ~/.ssh/ipaddr
fi

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

#navigation aliase
alias .="cd"
alias ..="cd .."
alias ...="cd ../../"

#git aliase
alias addall='git add .'
alias commit='git commit -m'
alias push='git push origin'
alias dotfiles='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

#system aliases
alias prf='poweroff'
alias rbt='reboot'
alias fyi='~/Downloads/raw/my.sh'

#pacman aliases
alias pacsrc='pacman -Ss'
alias pacins='sudo pacman -S'
alias pacup='sudo pacman -Syyu'

#abbrevations to open configs in nvim
alias cala="nvim ~/.config/alacritty/alacritty.yml"
alias czs="nvim ~/.zshrc"
alias cqt="nvim ~/.config/qtile/config.py"
alias cvi="nvim ~/.config/nvim/init.vim"
alias cmo="nvim ~/.xmonad/xmobarrc"
alias cmd="nvim ~/.xmonad/xmonad.hs"

#abbrevations to cd into dir
alias fala="cd ~/.config/alacritty/"
alias fqt="cd ~/.config/qtile/"
alias fvi="cd ~/.config/nvim/"
alias fmd="cd ~/.xmonad/"


if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then exec startx; fi
export PATH=~/bin:$PATH
